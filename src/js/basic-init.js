$(document).ready(function () {
    'use strict';

    // fancybox
    $('.fbox').fancybox();

    // masked input
    $('input[type="tel"]').mask('+38 (999) 999-99-99');

    const is_mobile = isMobile();

    if (is_mobile) document.body.classList.add('is-mobile');

    // menu stuff
    let mainNav = document.querySelector('.main-navigation') || false;

    if (mainNav) {
        // sticky menu
        let mainNavWrapper = document.querySelector('.main-navigation__wrapper');

        let waypoint = new Waypoint({
            element: mainNav,
            handler: function (direction) {
                if (direction === 'down') {
                    mainNavWrapper.classList.add('main-navigation__wrapper--sticky');
                    mainNav.style.height = mainNavWrapper.offsetHeight + 'px';
                } else {
                    mainNavWrapper.classList.remove('main-navigation__wrapper--sticky');
                    mainNav.style.height = 'auto';
                }
            }
        });

        // hamburger menu show/hide
        $('.main-navigation__trigger').on('click', function () {
            $('.main-navigation__box').fadeToggle(300);
        });

        if (is_mobile) {
            // fade mobile menu on click
            $('.main-navigation__list a').on('click', function () {
                $('.main-navigation__box').fadeOut(300);
            });
        }
    }

    // smooth page scrolling
    $('.scrollto').click(function () {
        var elementClick = '#'+$(this).attr('href').split('#')[1];
        var destination = $(elementClick).offset().top;
        jQuery('html:not(:animated),body:not(:animated)').animate({scrollTop: destination - 40}, 800);
        return false;
    });

    // our work — show more information
    if (is_mobile) {
        let workImgBox = $('.work__img-box');

        workImgBox.on('click', function () {
            let work = $(this).closest('.work');
            let workContent = work.find('.work__content');
            let workMobileMore = work.find('.work__mobile-more');

            work.toggleClass('work--active');
            workContent.fadeToggle(300);
            workMobileMore.text(work.hasClass('work--active') ? 'свернуть' : 'подробнее');
        });
    }

    // our work — open more works
    let buttonShowMoreWork = $('.our-work__button-more');
    let moreWorksBox = $('.our-work__work-list:last-of-type');

    buttonShowMoreWork.on('click', function () {
        moreWorksBox.fadeToggle(300);
        setTimeout(function () {
            buttonShowMoreWork.text(moreWorksBox.is(':visible') ? 'Свернуть' : 'Показать ещё работы');
        }, 400);
    });

    // video
    function findVideos() {
        let videos = document.querySelectorAll('.video');

        for (let i = 0; i < videos.length; i++) {
            setupVideo(videos[i]);
        }
    }

    function setupVideo(video) {
        let link = video.querySelector('.video__link');
        let media = video.querySelector('.video__media');
        let button = video.querySelector('.video__button');
        let id = parseMediaURL(media);

        video.addEventListener('click', () => {
            let iframe = createIframe(id);

            link.remove();
            button.remove();
            video.appendChild(iframe);
        });

        link.removeAttribute('href');
        video.classList.add('video--enabled');
    }

    function parseMediaURL(media) {
        let regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
        let url = media.src;
        let match = url.match(regexp);

        return match[1];
    }

    function createIframe(id) {
        let iframe = document.createElement('iframe');

        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('allow', 'autoplay');
        iframe.setAttribute('src', generateURL(id));
        iframe.classList.add('video__media');

        return iframe;
    }

    function generateURL(id) {
        let query = '?rel=0&showinfo=0&autoplay=1';

        return 'https://www.youtube.com/embed/' + id + query;
    }

    findVideos();



    // letter slider
    $('.letters__slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button class="letter__arrow letter__arrow_back" type="button"><span class="visually-hidden">Назад</span></button>',
        nextArrow: '<button class="letter__arrow letter__arrow_next" type="button"><span class="visually-hidden">Вперед</span></button>',
        responsive: [
            {
                breakpoint: 1290,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('.letters__slider').on('swipe', function(event, slick, direction) {});

    // services open/close
    let serviceName = $('.service__name');
    serviceName.on('click', function () {
        let service = $(this).parent();
        service.toggleClass('service--active');
        service.find('.service__content').fadeToggle(300);
    });

    // Send callback / Send request / Buy product
    jQuery("body").delegate( ".btn-order", "click", function(e) {
        e.preventDefault();

        var request_frm = jQuery(this).parents("form");
        jQuery(request_frm).find(".error-msg").hide();

        var errorFlag = 0;
        jQuery(request_frm).find(".rqf").removeClass("error-fld").each(function() {
            if ( jQuery(this).val() == "" || ( jQuery(this).hasClass("fld-email") && !isEmail(jQuery(this).val()) )) {
                errorFlag ++;
                jQuery(this).addClass("error-fld");
            }
        });

        if ( !errorFlag ) {

            var btnDefaultHTML = '<span id="job-caption">Заказать отчет с зарплатами</span><span class="order-form__job-name" id="job-name"></span>';
            var btnHTML = $(".order-form__button").html();
            var reqForm = $(request_frm)[0];
            var formData = new FormData(reqForm);

            $.ajax({
                url: '/sendrequest/',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend: function(){
                    $(".order-form__button").prop('disabled', true).html('<span id="job-caption">Отправка...</span>');
                },
                success: function (data) {
                    ym(52173394, 'reachGoal', 'success');
                    gtag('event', 'success', {'event_category': 'form', 'event_label': 'Success Form'});
                    fbq('track', 'Salary Report Request');
                    //console.log( data );

                    if( data == "1" ) {
                        $.fancybox.open({
                            src: '<h4 class="success">Спасибо, за Ваше обращение. Мы свяжемся с Вами в скором времени</h4>',
                            type : 'html',
                            helpers: {overlay: {locked: false}}
                        });
                        jQuery(request_frm).find(".rqf").each(function(){ jQuery(this).val(""); });
                    } else {
                        $.fancybox.open({
                            src: '<h4 class="error">Произошла ошибка при отправке заявки. Повторите отправку или перезвоните нам!</h4>',
                            type : 'html',
                            helpers: {overlay: {locked: false}}
                        });
                    }

                    // enable button
                    $(".order-form__button").prop('disabled', false).html(btnDefaultHTML);
                }
            });

        } else {
            jQuery(request_frm).find(".error-msg").show();
        }
    });

    function isMobile() {
        return $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function numberWithSpaces(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }

}); // end ready
