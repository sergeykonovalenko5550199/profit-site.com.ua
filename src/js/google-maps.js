function initMap() {
    
    let element = document.getElementById('google-map');

    let options = {
        zoom: 17,
        center: {lat:50.420413, lng:30.521000},
        scrollwheel: false
    };
    
    // maps initialization
    let myMap = new google.maps.Map(element, options);

    // marker and info-window
    let marker = new google.maps.Marker({
        position: {lat:50.420213, lng:30.522747},
        map: myMap,
        icon: 'img/base/marker.svg'
    });

    let InfoWindow = new google.maps.InfoWindow({
        content: '<div class="info-window"><strong>FOLGIN AGENCY</strong><p>Прибыльные сайты для бизнеса<br> с гарантией результата</p><a href="tel:0800218518">0 800 218 518</a></div>'
    });

    marker.addListener('click', function () {
        InfoWindow.open(myMap, marker);
    });
}
